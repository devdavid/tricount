use std::process;

use tricount;

fn main() {
    // Récupération des arguments
    let app_cli = tricount::AppCli::build().unwrap_or_else(|err| {
        eprintln!("{err}");
        process::exit(1);
    });

    if let Err(e) = tricount::run(app_cli) {
        eprintln!("Application error: {e}");
        process::exit(1);
    }
}
