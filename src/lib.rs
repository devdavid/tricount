use clap::{Parser, Subcommand};
use clap_verbosity_flag::{InfoLevel, Verbosity};
use log;
use std::error::Error;

/// Version rust de Tricount
#[derive(Parser)]
pub struct AppCli {
    /// Fix la verbosité de l'application (InfoLevel par défaut)
    #[clap(flatten)]
    verbose: Verbosity<InfoLevel>,

    #[command(subcommand)]
    command: AppCliCommands,
}

#[derive(Subcommand)]
pub enum AppCliCommands {
    /// Add a transaction
    #[command(arg_required_else_help = true)]
    Add {
        /// Name of the "Paid by" person
        #[arg(value_name = "PayedBy")]
        paid_by: String,

        /// Amount of the transaction
        #[arg(value_name = "Amount")]
        amount: f32,

        /// List of people concerned by the transaction (Use '*' for everyone)
        #[arg(value_name = "Whom1")]
        #[arg(required = true)]
        for_whom_list: Vec<String>,
    },
    /// Show balance
    #[command()]
    Show {},
    /// Suggest reimbursements
    #[command()]
    Suggest {},
}

impl AppCli {
    pub fn build() -> Result<AppCli, &'static str> {
        let app_cli = AppCli::parse();

        // Configure the logger
        env_logger::Builder::new()
            .filter_level(app_cli.verbose.log_level_filter())
            .init();

        // Return configuration
        Ok(app_cli)
    }
}

fn add_command_handler(paid_by: String, amount: f32, for_whom_list: Vec<String>) {
    let mut is_for_everyone = false;

    if for_whom_list.iter().any(|e| e == "*") {
        is_for_everyone = true;
    }

    if is_for_everyone {
        log::info!("{0} payed {1}€ for {2:?}", paid_by, amount, for_whom_list);
    } else {
        log::info!("{0} payed {1}€ for everyone", paid_by, amount);
    }
}

fn show_command_handler() {
    log::info!("Balance:")
}

fn suggest_command_handler() {
    log::info!("Suggested reimbursements:")
}

pub fn run(app_cli: AppCli) -> Result<(), Box<dyn Error>> {
    match app_cli.command {
        AppCliCommands::Add {
            paid_by,
            amount,
            for_whom_list,
        } => add_command_handler(paid_by, amount, for_whom_list),
        AppCliCommands::Show {} => show_command_handler(),
        AppCliCommands::Suggest {} => suggest_command_handler(),
    }
    Ok(())
}
